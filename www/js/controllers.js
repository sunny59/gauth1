angular.module('starter.controllers', ['firebase'])

.controller('LoginCtrl', ["$scope", "$firebaseAuth", function($scope, $firebaseAuth) {
    var auth = $firebaseAuth();
    $scope.login = function() {
        console.log('login clicked');

        $scope.firebaseUser = null;
        $scope.error = null;

        var provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithRedirect(provider);

        firebase.auth().getRedirectResult().then(function(result) {
            if (result.credential) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // ...
            }
            // The signed-in user info.
            var user = result.user;
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });

    };


    auth.$onAuthStateChanged(function(firebaseUser) {
      $scope.firebaseUser = firebaseUser;
      window.localStorage.setItem('firebaseUser',angular.toJson(firebaseUser));
    });

    $scope.logout = function(){
        console.log('logout clicked');
        window.localStorage.removeItem('firebaseUser');
        firebase.auth().signOut().then(function() {
            console.log('signout success');
        }).catch(function(error) {
            console.log('error');
        });
    };

    $scope.backup = function(){
        console.log('backup clicked');
       var user = window.localStorage.getItem('firebaseUser');
       console.log(angular.fromJson(user));
    };

}])

.controller('IntroCtrl', function($scope, $state, $ionicSlideBoxDelegate) {

    $scope.$on('$ionicView.enter', function() {
            $ionicSlideBoxDelegate.update();
            console.log("scope for intro is on");
        })
        // Called to navigate to the main app
    $scope.startApp = function() {
        $state.go('login');
    };
    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
    };

    // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
    };
});